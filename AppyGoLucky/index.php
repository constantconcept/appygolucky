<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Appy go lucky</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="css/gm-v=1.0.css" rel="stylesheet">
  <link href="css/app.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

  <meta name="theme-color" content="#ffffff">
  
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


  <script src="js/bootstrap.min.js"></script>
  <script src="js/parallax.min.js"></script>
  <!-- <script src="js/main.js"></script> -->


</head>
<body class="gm-homepage">

<div class="video-overlay"></div>

<div id="video-background" style="height:100%;" >
 
  <div id="player"></div>
  <video autoplay loop id="video" muted plays-inline style="top:0px;">
    <source src="images/file.mp4" type="video/mp4" poster="">
  </video>

</div>

<section id="hero">
<div class="jumbotron">
    <div class="container-fluid">
      <div class="navbar navbar-default navbar-static-top" role="navigation">
          <div class="container">
       
              <div class="navbar-header">
                  <a class="navbar-brand" href="index.html" title="Gamemine"><img src="images/logogz.png" alt="Gamemine logo"></a>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <div class="gm-toggle-wrapper">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </div>
                  <span class="menu-text">MENU</span>
                  </button>
              </div>
       
        
       
          </div>
      </div>
      <div class="row hero-arrow"><a href="index.html#titles"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></div>
    </div>
</div>
</section>

<section id="titles">
    <div class="container-fluid">
      <div class="row gm-section-title">
        <h2>titles.</h2>
      </div>
      <div class="row app-title-box">
          <div class="col-md-4">
            <div class="gm-product gm-product-1">
            <a href="" title="Night Racer">The Last Stand</a>
            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="gm-product gm-product-2">
            <a href="" title="The War of Ages">House of 1000 Doors</a>

            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="gm-product gm-product-3">
            <a href="" title="FRED">Legions of Rome</a>
            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
        </div>

        <div class="row app-title-box">
          <div class="col-md-4">
            <div class="gm-product gm-product-1">
            <a href="" title="Night Racer">The Last Stand</a>
            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="gm-product gm-product-2">
            <a href="" title="The War of Ages">House of 1000 Doors</a>

            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="gm-product gm-product-3">
            <a href="" title="FRED">Legions of Rome</a>
            </div>
            <div>
            <a href="https://itunes.apple.com/app/id1435120620">
            	<img src="./images/btn-app-store.svg" class="app-button" alt="Download on the App Store"></a>
            	<a href="https://play.google.com/store/apps/details?id=com.game5mobile.lineandwater"><img src="./images/btn-google-play.svg" class="app-button" alt="Get it on Google Play"></a>
            </div>
          </div>
        </div>
  </div>
  <div class="slider-box">
      <div class="slider">
	  <div class="banner-img">
	  	<img data-lazy="https://lionstudios.cc/wp-content/uploads/2018/05/LionStudios_Website_HOME_CarouselMocks_@2x_BigBigBaller.jpg" alt="Big Big Baller" title="">
	  </div>
	  <div class="banner-img"><img data-lazy="https://lionstudios.cc/wp-content/uploads/2018/05/hero-bg-love-balls-full.jpg" alt="Love Balls" title=""></div>
	  <div class="banner-img"><img data-lazy="https://lionstudios.cc/wp-content/uploads/2018/05/hero-bg-love-balls-full.jpg" alt="Love Balls" title=""></div>
	</div>	
       </div>
        
    </div>
  </section>  

  <section class="gm-background" data-parallax="scroll" data-image-src="./images/background2.jpg" data-z-index="1000">
  </section>

  <section id="about">
  <div class="container">  
    <div class="row gm-section-title" style="margin: 0px;">
    <div class="col-md-12" >
      <h2>about.</h2>
    </div>
    </div>
    <div class="row" style="margin: 0px;">
    <div class="col-md-12" >
        <p>Game Zone is your premium source for online games, apps, ringtones, e-books and more. Game Zone provides our customer with access to hundreds of new content each week.  Our back end online portal is optimized for mobile and desktop and serves millions of clients from around the world. 
        </p>

        <p>Game Zone also creates proprietary content, games, ebooks and more to it's customers.
        </p>
    </div>
    </div>
  </div>
  </section>

  <section class="gm-background mobile-auto-height" data-parallax="scroll" data-image-src="./images/GM_BlockFraud_BW.jpg" data-z-index="1000" style="position: relative;z-index: 99999;opacity: 1;visibility: visible;">
        <div style="display: inline-block;vertical-align: middle;width: 0%;height: 100%;"></div
          ><div style="background-color:rgba(255,255,255,0.65); display: inline-block;vertical-align: middle;width: 100%;">
           
        </div> 
  </section>

  <!-- <section class="gm-background" data-parallax="scroll" data-image-src="./images/background3.jpg" data-z-index="1000"></section> -->
   
 
  <section id="contact">
  <div class="container">  
    <div class="row gm-section-title">
      <div class="col-md-12">
      <h2>contact.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div id="mc_embed_signup">
          <p id="msgSubmit" class="hidden alert text-center">Message sent! <br/> We'll be back to you shortly! <br/><br/> <strong>Thank you!</strong></p>
          <form id="contact-form" name="mc-embedded-subscribe-form" class="validate gm-contact-form">
              <div id="mc_embed_signup_scroll">
            
          <div class="mc-field-group">
            <label for="mce-FNAME">First Name </label>
            <input type="text" value="" name="fname" placeholder="First Name" required>
          </div>
          <div class="mc-field-group">
            <label for="mce-LNAME">Last Name </label>
            <input type="text" value="" name="lname"  placeholder="Last Name" required>
          </div>
          <div class="mc-field-group">
            <label for="mce-EMAIL">Email Address </label>
            <input type="email" value="" name="email" placeholder="Email" required>
          </div>
          <div class="mc-field-group">
            <label for="mce-MMERGE3">Subject </label>
            <input type="text" value="" name="subject"  placeholder="Subject" required>
          </div>
          <div class="mc-field-group">
            <label for="mce-MMERGE4">Message </label>
            <textarea type="text" value="" name="message"  placeholder="Message" rows="5" required></textarea>
          </div>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div class="clear">
                <input type="submit" value="Send" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
              </div>
          </form>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>
  </div>
  
      
        <div id="SantaMonica-map" class="map" style="min-height: 400px;"></div>
      </div>
      <div class="col-md-4 map-container">
        <div id="ClujNapoca-map" class="map" style="min-height: 400px;"></div>
      </div>
    </div>
  </section>

  <footer id="gm-footer">
    <div class="container">
      <div class="row gm-footer-row" style="margin: 0px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <ul class="gm-footer-menu">
          
          </ul>
        </div>
        <div class="col-md-2"></div>
      </div>
      <div class="row gm-footer-row" style="margin: 0px;">
      <div class="col-md-2"></div>
      <div class="col-md-4">
          <p class="gm-copyright">&copy; 2018 Game Zone. All rights reserved.</p> 
      </div>
      
      <div class="col-md-2"></div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script type="text/javascript">
  
    $('#contact-form').on('submit', function() {
        event.preventDefault();
        submitForm();
     });
     function submitForm(){
        // Initiate Variables With Form Content
        // var name = $("#name").val();
        // var email = $("#email").val();
        // var message = $("#message").val();
        // var reason = $("#reason").val();
        $('#msgSubmit').addClass('hidden');
        $.ajax({
            type: "POST",
            url: "/send.php",
            data: $('#contact-form').serialize(),
            success : function(text){
                formSuccess(text);
            }
        });
    }
    function formSuccess(text){
        $( "#msgSubmit" ).removeClass( "hidden" );
        if (text == 'success') {
          $( "#msgSubmit" ).html("Message sent! <br/> We'll be back to you shortly! <br/><br/> <strong>Thank you!</strong>").removeClass( "hidden" ).addClass('alert-success');
        } else {
          $( "#msgSubmit" ).html("Something went wrong! <br/> We'll be back to you shortly! <br/><br/> <strong>Thank you!</strong>").removeClass( "hidden" ).addClass('alert-danger');
        }
    }
    $(document).ready(function(){
    	var sliderEnabled = false;
    	$(".slider-box").hide();
    	$(".app-title-box").show();
    	if (sliderEnabled) {
			$(".slider-box").show();
			$(".app-title-box").hide();
    		$('.slider').slick({
			  	lazyLoad: 'ondemand',
				  dots: true,
				  infinite: true,
				  autoplay : true,
				  autoplaySpeed : 3000,
				  slidesToShow: 1,
				  adaptiveHeight: false,
				  speed: 500,
				  fade: true,
				  cssEase: 'linear'
				});
    	}
	  
	});
  </script> 
	
</body>
</html>